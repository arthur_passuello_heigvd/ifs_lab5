/**************************************************************************
 * HEIG-VD, Institut REDS
 *
 * File       : lab5_gensignal_fpga.c
 * Author     : Alexandre MALKI
 * Modifications : Marie Lemdjo & Arthur Passuello
 * Created on : 24.05.2017
 *
 * Description  : Programme permettant de generer une funcion
 *
 * Ver  Date        Author     Description
 * 0.1  24.05.2017  AMX	       Creation du laboratoire generateur de signaux
 *
 ************************************************************************** 
 */

#include <common.h>

//Masque des registres de la SP6
#define FPGA_SP6_MSK_SWX	0xFF
#define FPGA_SP6_MSK_SW3	0x04
/* Il s'agit du masque pour le segment G de la FPGA */
#define FPGA_SP6_SEG_G		0x40

/* Il s'agit d'un mask indiquant pour supprimer la valeur de G et DP */
#define FPGA_SP6_MSK_G_DP	0x3F

//Les acces a la FPGA sont sur 16 bits. Il faut utiliser un type short
//Le type volatile indique que cette donnee ne peut pas etre mise dans le cash
#define FPGA_SP6_BASE_ADDR      0x19000000

#define FPGA_SP6_CST            *(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0x0)
#define FPGA_SP6_SWX			*(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0x2 )
#define FPGA_SP6_LED			*(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0x4 )
#define FPGA_SP6_7SEG			*(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0x6 )
#define FPGA_SP6_DPSW			*(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0x8 )
#define FPGA_SP6_CMD			*(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0xA )
#define FPGA_SP6_CONS			*(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0xC )
//Acces à la FPGA pour la lecture du signal busy
#define FPGA_SP6_RD_BUSY		*(volatile unsigned short *)(FPGA_SP6_BASE_ADDR + 0xE )


// Define base amp
#define DAC_BASE_AMP 		4095

//Masque pour les dips-witchs 1 & 2
#define FPGA_SP6_MSK_DPSW_2_3   0x3



/****************** DAC signal defines ***************/

/****************** Masks FPGA **********************/
#define FPGA_SP6_SW1		1 // src[0]
#define FPGA_SP6_SW2		2 // src[1]
#define FPGA_SP6_SW3		4 // freq
#define FPGA_SP6_SW4		8 // amp[0]
#define FPGA_SP6_SW5		16 // amp[1]

// Defines pour les sources
#define SRC_CST				0
#define SRC_SAW				1
#define SRC_ENC				2
#define SRC_CPU				3

// Defines poour les gains
#define NBR_BITS_FRACT			3
#define INCR_MIN				2

//Valeurs maximale et minimale du signal
#define V_MAX		0XFFF
#define V_MIN		0X000

/***************** Internal defined for application ***/
//adresse pour les acces de SW4 du CPU
#define GPIO_DATAIN_REGISTER_BANK4  *(volatile unsigned int *) (0x49054038)
#define MASK_SW_4                   0x40000000



void consigne(unsigned short freq, unsigned short amp);
void send_value_dac(unsigned short value);

//Fonctions utilitaires
void write_consigne(unsigned short value);
void send_cmd(unsigned short command);



//Fonction main
int reptar_io_fpga(void)
{
	printf("////Uboot: Reptar gensignal lab5\n");

	printf("////Constante: 0x%04X\n", FPGA_SP6_CST);

/*Quitte l'application quand le SW4 du CPU est presse*/
	while (!(GPIO_DATAIN_REGISTER_BANK4 & MASK_SW_4)) {
		//Récupération de la valeur des dip-switchs 2 et 3
		volatile unsigned short dipswValue = FPGA_SP6_DPSW & (FPGA_SP6_SW1 | FPGA_SP6_SW2);
		//Si la valeur de la source est différente de 3, on teste les 3 premiers modes
		if(dipswValue != 0x3 ){	
			
			send_cmd(dipswValue);
		//Sinon on cree le signal triangulaire en incrémentant ou en décrémentant la valeur de la consigne
		}else{
			send_cmd(dipswValue);
			//valeur de la fréquence
			unsigned short freq = (FPGA_SP6_DPSW & FPGA_SP6_SW3) >> 2;

			//valeur du gain
			unsigned short gain = (FPGA_SP6_DPSW & (FPGA_SP6_SW4 | FPGA_SP6_SW5)) >> 3;
 
			do_consigne(freq, gain);	
		}
			
	}

	return 0;
}

/*void send_value_dac(unsigned short value)
{	
	write_consigne(value);
 
	FPGA_SP6_CMD = 0x7;
	
	//Waiting for busy
	while( FPGA_SP6_RD_BUSY & 0x1 );

	//aquittement du signal busy
	FPGA_SP6_CMD |= 0x80;
	FPGA_SP6_CMD &= ~0X1;
	
	//Check the spi line
	while( FPGA_SP6_CMD & 0x1);
	
}*/


void send_value_dac(unsigned short value)
{	
	while(FPGA_SP6_RD_BUSY);
	write_consigne(value);
	FPGA_SP6_CMD |= 0x1;
	FPGA_SP6_CMD &= ~ 0x1;

}

void do_consigne(unsigned short freq, unsigned short amp_i)
{
    unsigned short amp = (((amp_i + 1) * DAC_BASE_AMP) << NBR_BITS_FRACT ) / 4;
    unsigned short step = (INCR_MIN + amp_i*INCR_MIN) >> freq;

	unsigned short i = 0;
	for(; i < amp; i += step) {
		send_value_dac(i >> NBR_BITS_FRACT);
	}
	for(; i > 0; i -= step) {
		send_value_dac(i >> NBR_BITS_FRACT);
	}
}

void send_cmd(unsigned short dipswValue) 
{

	//Préparation de la commande : // activation du signal SPI et copie de la sélection source
	//On effectue un décale à gauche pour car les valeurs de dipsw sont situés sur les bits 2 et 3 
	volatile unsigned short command = (dipswValue << 0x1) | 0x1;

	 // envoie de la commande
	 FPGA_SP6_CMD = command;
}

void write_consigne(unsigned short value) 
{ 
      FPGA_SP6_CONS = (value);
}




